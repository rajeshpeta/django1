

from django.http import HttpResponse
from django.shortcuts import render

def travello(request):
    return render(request, 'travello.html')